<?php

/**
 * @file
 * Footer template.
 */

global $theme;
?>
<div class="breakpoint"></div>
<div id="site" data-nav-visible="false" class="<?php print $classes; ?> uw-site"<?php print $attributes; ?>>
  <div class="uw-site--inner">
    <div class="uw-section--inner">
      <div id="skip" class="skip">
        <a href="#main" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to main'); ?></a>
        <a href="#footer" class="element-invisible element-focusable uw-site--element__invisible uw-site--element__focusable"><?php print t('Skip to footer'); ?></a>
      </div>
    </div>
    <?php
      // For publication theme:
      // - Remove class uw-header--global to allow no big space between header
      //   and color bar.
      // - Remove class uw-section--inner to allow the header screen wide.
      if ($theme == 'uw_theme_publication'): ?>
      <div id="header">
        <div>
          <?php print render($page['global_header']); ?>
        </div>
      </div>
    <?php else: ?>
    <div id="header" class="uw-header--global">
      <div class="uw-section--inner">
        <?php print render($page['global_header']); ?>
      </div>
    </div>
    <?php endif; ?>
    <div id="site--offcanvas" class="uw-site--off-canvas">
      <div class="uw-section--inner">
        <?php print render($page['site_header']); ?>
      </div>
    </div>
    <div id="site-header" class="uw-site--header">
      <div class="uw-section--inner">
        <a href="<?php print $front_page ?>" title="<?php print $site_name; ?>" rel="home">
          <?php print $site_name; ?>
        </a>
      </div>
    </div>
    <div class="uw-header--banner__alt">
      <div class="uw-section--inner">
        <?php print render($page['banner_alt']); ?>
      </div><!-- /uw-headeranner__alt -->
    </div>
    <div id="main" class="uw-site--main">
      <div class="uw-section--inner">
        <div id="site-navigation-wrapper" class="uw-site-navigation--wrapper">
          <div id="site-specific-nav" class="uw-site-navigation--specific">
            <div id="site-navigation" class="uw-site-navigation">
              <?php if (user_access('access content')) {
                print render($page['sidebar_first']);
              } ?>
            </div>
          </div>
        </div>
        <div class="uw-site--main-top">
          <div class="uw-site--banner">
            <?php print render($page['banner']); ?>
          </div>
          <div class="uw-site--messages">
            <?php print $messages; ?>
          </div>
          <div class="uw-site--help">
            <?php print render($page['help']); ?>
          </div>
          <div class="uw-site--breadcrumb">
            <?php print $breadcrumb; ?>
          </div>
          <div class="uw-site--title">
            <?php list($title, $content) = uw_fdsu_theme_separate_h1_and_content($page, $title); ?>
            <h1><?php print $title ? $title : $site_name; ?></h1>
          </div>
        </div>
        <div class="uw-site-main--content">
          <div id="content" class="uw-site-content">
          </div><!--/main-content-->
          <?php $sidebar = render($page['sidebar_second']); ?>
          <?php $sidebar_promo = render($page['promo']); ?>
          <?php if (isset($sidebar) || isset($sidebar_promo) && ($sidebar !== NULL && $sidebar !== '') || ($sidebar_promo !== NULL && $sidebar_promo !== '')) { ?>
            <div id="site-sidebar-wrapper" class="uw-site-sidebar--wrapper">
              <div id="site-sidebar-nav" class="uw-site-sidebar--nav">
                <div id="site-sidebar" class="uw-site-sidebar <?php
                  if ($sidebar_promo !== NULL && $sidebar_promo !== '') {
                    echo ('sticky-promo');
                  }
                ?>">
                  <?php if (isset($sidebar_promo) && $sidebar_promo !== NULL && $sidebar_promo !== '') { ?>
                    <div class="uw-site-sidebar--promo">
                      <?php print render($page['promo']); ?>
                    </div>
                  <?php } ?>
                  <div class="uw-site-sidebar--second">
                    <?php print render($page['sidebar_second']); ?>
                  </div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div><!--/section inner-->
    </div><!--/site main-->
    <div id="footer" class="uw-footer">
      <div id="uw-site-share" class="uw-site-share">
        <div class="uw-section--inner">
          <div class="uw-section-share"></div>
          <ul class="uw-site-share-top">
            <li class="uw-site-share--button__top">
              <a href="#main" id="uw-top-button" class="uw-top-button">
                <span class="ifdsu fdsu-arrow"></span>
                <span class="uw-footer-top-word">TOP</span>
              </a>
            </li>
            <li class="uw-site-share--button__share">
              <a href="#uw-site-share" class="uw-footer-social-button">
                <span class="ifdsu fdsu-share"></span>
                <span class="uw-footer-share-word">Share</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div id="site-footer" class="uw-site-footer open-site-footer">
        <div class="uw-section--inner">
          <?php if (empty($page['site_footer'])): ?>
            <div class="uw-site-footer1 uw-no-site-footer">
          <?php else : ?>
            <div class="uw-site-footer1">
          <?php endif; ?>

          <div class="uw-site-footer1--logo-dept">
            <?php
              $site_logo_info = variable_get('uw_site_logo_info');
              $facebook = variable_get('uw_nav_site_footer_facebook');
              $twitter = variable_get('uw_nav_site_footer_twitter');
              $instagram = variable_get('uw_nav_site_footer_instagram');
              $youtube = variable_get('uw_nav_site_footer_youtube');
              $linkedin = variable_get('uw_nav_site_footer_linkedin');
              $snapchat = variable_get('uw_nav_site_footer_snapchat');

              $alt_tag = str_replace('_', ' ', variable_get('uw_nav_site_footer_logo')) . ' logo';
              $arr = explode(" ", $alt_tag);
              $arr = array_unique($arr);
              $alt_tag = implode(" ", $arr);
            ?>
            <?php if ($site_logo_info) : ?>
              <a href="<?php print $site_logo_info['link']; ?>"><img src="<?php print $site_logo_info['logo']; ?>" alt="<?php print $site_logo_info['alt']; ?>" /></a>
            <?php else : ?>
              <?php $site_name = (strtolower($site_name)); print '<a href="' . url('<front>') . '">' . (ucfirst($site_name)) . '</a>'; ?>
            <?php endif; ?>
          </div>
          <div class="uw-site-footer1--contact">
            <ul class="uw-footer-social">
              <?php if ($facebook !== "" && $facebook !== NULL): ?>
                <li>
                  <a href="https://www.facebook.com/<?php print check_plain($facebook); ?>" aria-label="facebook">
                    <span class="ifdsu fdsu-facebook"></span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ($twitter !== "" && $twitter !== NULL): ?>
                <li>
                  <a href="https://www.twitter.com/<?php check_plain(print $twitter); ?>" aria-label="twitter">
                    <span class="ifdsu fdsu-twitter"></span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ($youtube !== "" && $youtube !== NULL): ?>
                <li>
                  <a href="https://www.youtube.com/<?php check_plain(print $youtube); ?>" aria-label="youtube">
                    <span class="ifdsu fdsu-youtube"></span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ($instagram !== "" && $instagram !== NULL): ?>
                <li>
                  <a href="https://www.instagram.com/<?php print check_plain($instagram); ?>" aria-label="instagram">
                    <span class="ifdsu fdsu-instagram"></span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ($linkedin !== "" && $linkedin !== NULL): ?>
                <li>
                  <a href="https://www.linkedin.com/<?php check_plain(print $linkedin); ?>" aria-label="linkedin">
                    <span class="ifdsu fdsu-linkedin"></span>
                  </a>
                </li>
              <?php endif; ?>
              <?php if ($snapchat !== "" && $snapchat !== NULL): ?>
                <li>
                  <a href="https://www.snapchat.com/add/<?php check_plain(print $snapchat); ?>" aria-label="snapchat">
                    <span class="ifdsu fdsu-snapchat"></span>
                  </a>
                </li>
              <?php endif; ?>
            </ul>
          </div>
          <?php $node = menu_get_object() ?>
          <?php if ($node->type == 'uw_site_footer'): ?>
            <div class="uw-site-footer2">
              <?php if ($tabs): ?>
                <div class="node-tabs"><?php print render($tabs); ?></div>
              <?php endif; ?>
              <?php
              unset($page['content']['uw_social_media_sharing_social_media_block']);
              print render($page['content']['workbench_block']);
              if ($node) {
                $nid = $node->nid;
                if (isset($page['content']['system_main']['nodes'][$nid]['field_body_no_summary']['#items'][0])) {
                  print render($page['content']['system_main']['nodes'][$nid]['field_body_no_summary']);
                }
              }
              ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <?php print render($page['global_footer']); ?>
    </div>
  </div>
</div><!--/site-->
<div class="ie-resize-fix"></div>
