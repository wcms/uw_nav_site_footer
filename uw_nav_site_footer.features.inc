<?php

/**
 * @file
 * uw_nav_site_footer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_nav_site_footer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_nav_site_footer_node_info() {
  $items = array(
    'uw_site_footer' => array(
      'name' => t('Site footer'),
      'base' => 'node_content',
      'description' => t('A footer which appears across all pages on your website.'),
      'has_title' => '1',
      'title_label' => t('Title (admin use only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
