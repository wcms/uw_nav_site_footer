<?php

/**
 * @file
 * uw_nav_site_footer.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_nav_site_footer_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_footer';
  $context->description = 'Displays the local site footer.';
  $context->tag = 'Navigation';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_nav_site_footer-site-footer' => array(
          'module' => 'uw_nav_site_footer',
          'delta' => 'site-footer',
          'region' => 'site_footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Displays the local site footer.');
  t('Navigation');
  $export['site_footer'] = $context;

  return $export;
}
