<?php

/**
 * @file
 * uw_nav_site_footer.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_nav_site_footer_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_site_footer_social_media|node|uw_site_footer|form';
  $field_group->group_name = 'group_site_footer_social_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_site_footer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social Media',
    'weight' => '8',
    'children' => array(
      0 => 'field_site_footer_facebook',
      1 => 'field_site_footer_instagram',
      2 => 'field_site_footer_linked_in',
      3 => 'field_site_footer_twitter',
      4 => 'field_site_footer_you_tube',
      5 => 'field_site_footer_snapchat',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-site-footer-social-media field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_site_footer_social_media|node|uw_site_footer|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Social Media');

  return $field_groups;
}
