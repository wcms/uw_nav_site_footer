<?php

/**
 * @file
 * uw_nav_site_footer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_nav_site_footer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_site_footer content'.
  $permissions['create uw_site_footer content'] = array(
    'name' => 'create uw_site_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_site_footer content'.
  $permissions['delete any uw_site_footer content'] = array(
    'name' => 'delete any uw_site_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_site_footer content'.
  $permissions['delete own uw_site_footer content'] = array(
    'name' => 'delete own uw_site_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_site_footer content'.
  $permissions['edit any uw_site_footer content'] = array(
    'name' => 'edit any uw_site_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_site_footer content'.
  $permissions['edit own uw_site_footer content'] = array(
    'name' => 'edit own uw_site_footer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_site_footer revision log entry'.
  $permissions['enter uw_site_footer revision log entry'] = array(
    'name' => 'enter uw_site_footer revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer authored by option'.
  $permissions['override uw_site_footer authored by option'] = array(
    'name' => 'override uw_site_footer authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer authored on option'.
  $permissions['override uw_site_footer authored on option'] = array(
    'name' => 'override uw_site_footer authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer promote to front page option'.
  $permissions['override uw_site_footer promote to front page option'] = array(
    'name' => 'override uw_site_footer promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer published option'.
  $permissions['override uw_site_footer published option'] = array(
    'name' => 'override uw_site_footer published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer revision option'.
  $permissions['override uw_site_footer revision option'] = array(
    'name' => 'override uw_site_footer revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_site_footer sticky option'.
  $permissions['override uw_site_footer sticky option'] = array(
    'name' => 'override uw_site_footer sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
